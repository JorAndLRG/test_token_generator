import os
from flask import Flask
from app import app_bp
from models import db
from flask_cors import CORS

import firebase_admin
from firebase_admin import credentials


file_open = os.path.abspath(r'C:\Users\VFCONSULTING\Documents\ms\prueba\test_token_generator\json\credential.json')
cred = credentials.Certificate(file_open)
default_app = firebase_admin.initialize_app(cred)

app = Flask(__name__)
CORS(app)
app.config.from_object("config")
app.register_blueprint(app_bp, url_prefix='/public')
db.init_app(app)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

if __name__ == "__main__":
    app.run(debug=True)
