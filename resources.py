import logging as log
import constants as cons
import helpers

from datetime import datetime
from flask import jsonify, request
from flask_restful import Resource
from flask_httpauth import HTTPBasicAuth

from webargs import fields
from webargs.flaskparser import use_args

from models import Test_user, TestUserSchema
from controllers import UsertokenController, AppSecurityController
from errors import UserTokenControllerError, AppSecurityControllerError

auth = HTTPBasicAuth()

app_security_controller = AppSecurityController()
usertoken_controller = UsertokenController()

testuser_schema = TestUserSchema()
testusers_schema = TestUserSchema(many=True)


@auth.get_password
def get_password(oauth_user):
    try:
        resp = app_security_controller.get_password(oauth_user)
        log.debug('type res: {}'.format(type(resp)))
        log.debug('sending password ...')
        return resp

    except AppSecurityControllerError as e:
        log.error(e)
        return None


@auth.error_handler
def unauthorized():
    # return 403 instead of 401 to prevent browsers from displaying the default
    # auth dialog
    resp = helpers.standard_response(cons.HTTP_403_FORBIDDEN, None, cons.SECURITY_UNAUTHORIZED_MSG)
    return jsonify(resp)


# parameters from url
url_args = {
    'num_test_id': fields.Number()
}


class UserTokenResource(Resource):

    @use_args(url_args)
    @auth.login_required
    def get(self, url_args):
        # LISTADO DE NOTIFICACIONES PREDETERMINADASS
        try:
            log.info('------------ INICIANDO PROCESO DE LISTADO MS-NOTIFICATIONS ------------')
            url_args = dict((k, v) for k, v in url_args.items() if v is not "")
            log.info('Leyendo los datos de la tabla notifications')
            result = usertoken_controller.GETALL(url_args)
            log.info('Serializando datos')
            notification_serialized, notification_errs = testusers_schema.dump(result)

            log.debug('notification ID ->' + str(notification_serialized))
            log.info('------------ PROCESO DE LISTADO MS-NOTIFICATIONS TERMINADO ------------')
            content = {'notification': notification_serialized}
            response = helpers.standard_response(cons.HTTP_200_OK, content, None)
            return response, cons.HTTP_200_OK

        except UserTokenControllerError as e:
            log.info('ERROR: Se encontro un error durante la ejecucion --->' + e.message)
            response = helpers.standard_response(e.code_error, None, e.message)
            return response, e.code_error

    @auth.login_required
    def post(self):
        try:
            log.info('inserting notification ...')
            json_in = request.get_json(force=True)

            if json_in.get('user_token') is None:
                resp = helpers.standard_response(cons.HTTP_400_BAD_REQUEST, None, cons.BAD_REQUEST_MSG)
                return resp, cons.HTTP_400_BAD_REQUEST

            notification = json_in.get('user_token')
            log.info('Reading JSON')
            e = Test_user(
                str_token_user=notification.get('str_token_user'),
                dte_creationdate=datetime.utcnow()
            )

            log.debug('Executing insert for notification of -> ' + str(e.num_test_id))
            usertoken_controller.insert_notification(e)

            notification_serialized, notification_err = testuser_schema.dump(e)
            log.info('notification inserted!')
            content = {'notification': notification_serialized}
            response = helpers.standard_response(cons.HTTP_200_OK, content, None)
            return response, cons.HTTP_200_OK

        except UserTokenControllerError as e:
            response = helpers.standard_response(e.code_error, None, e.message)
            return response, e.code_error
