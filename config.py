import os
import logging

# bd config
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True

pg_connection_string = 'postgresql+psycopg2://{username}:{password}@{hostname}:{port}/{database}'

SQLALCHEMY_DATABASE_URI = pg_connection_string.format(
    username=os.environ.get('DBUSERNAME'),
    password=os.environ.get('DBPASSWORD'),
    hostname=os.environ.get('DBHOSTNAME'),
    port=os.environ.get('DBPORT'),
    database=os.environ.get('DBNAME')
)

SQLALCHEMY_POOL_SIZE = int(os.environ.get('DBPOOL_SIZE'))
SQLALCHEMY_POOL_TIMEOUT = int(os.environ.get('DBPOOL_TIMEOUT'))

# log config
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

SQLALCHEMY_RECORD_QUERIES = True
