from flask import Blueprint
from flask_restful import Api
from resources import UserTokenResource

app_bp = Blueprint('notification', __name__)
api = Api(app_bp)

# Route
api.add_resource(UserTokenResource, '/usertoken')
