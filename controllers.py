from __future__ import print_function
import logging as log
import requests
import json
import datetime
import constants as cons
import os

from firebase_admin import messaging
from models import db, Test_user
from errors import UserTokenControllerError, AppSecurityControllerError
from sqlalchemy.exc import SQLAlchemyError


class AppSecurityController:

    def __init__(self):
        pass

    def get_password(self, user_auth):
        try:
            json_in = {'oauth_user': {'user': user_auth, 'password': None}}
            service_resp = requests.post(os.environ.get('URL_OAUTH_SECURITY_SERVICE'), json=json_in)

            log.debug('Response security service: {}'.format(service_resp.status_code))

            if service_resp.status_code is not cons.HTTP_200_OK:
                raise AppSecurityControllerError(service_resp.status_code)

            json_out = json.loads(service_resp.text)
            content = json_out.get('content')

            if content is None:
                raise AppSecurityControllerError(cons.SECURITY_RESPONSE_MSG)

            oauth_user = content.get('oauth_user')

            if oauth_user is None:
                raise AppSecurityControllerError(cons.SECURITY_RESPONSE_MSG)

            log.debug("end get password")

            return oauth_user.get('str_oauth_cred_password')
        except requests.RequestException as e:
            raise AppSecurityControllerError(e)


class UsertokenController:

    def __init__(self):
        pass

    def GETALL(self, conditionals):
        log.info('Starting process')
        s = db.session
        try:
            query = s.query(Test_user.num_test_id,
                            Test_user.str_token_user,
                            Test_user.dte_creationdate)

            filters = None

            if bool(conditionals):
                filter_list = list()
                for key, val in conditionals.items():
                    if hasattr(Test_user, key):
                        filter_list.append(getattr(Test_user, key) == val)
                        log.debug('adding filter {} -> value {} ...'.format(key, val))

                filters = tuple(filter_list)

            query = query.all() if not filters else query.filter(*filters)

            log.debug(query)

            return query

        except SQLAlchemyError as e:
            log.error('SQLAlchemyError -> ' + str(e))
            raise UserTokenControllerError(e, cons.HTTP_500_INTERNAL_SERVER_ERROR)
        finally:
            s.close()

    def insert_notification(self, user_token):
        log.info('Starting process')
        s = db.session
        try:
            log.info('Starting process')
            s = db.session
            s.add(user_token)
            s.commit()

            log.info('notification with id {} was inserted'.format(user_token.num_test_id))
            return user_token

        except SQLAlchemyError as e:
            log.error('SQLAlchemyError -> ' + str(e))
            raise UserTokenControllerError(e, cons.HTTP_500_INTERNAL_SERVER_ERROR)
        finally:
            s.close()


