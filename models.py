from marshmallow import fields
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Sequence
from sqlalchemy.ext.declarative import declarative_base

ma = Marshmallow()
db = SQLAlchemy()
base = declarative_base()


class Test_user(db.Model):
    __tablename__ = 'test_user'
    sq_user_id = Sequence('sq_user_id', metadata=base.metadata)
    num_test_id = db.Column(db.Numeric, sq_user_id, server_default=sq_user_id.next_value(),
                            primary_key=True)

    str_token_user = db.Column(db.Text)
    dte_creationdate = db.Column(db.Date)


class TestUserSchema(ma.Schema):
    num_test_id = fields.Number()
    str_token_user = fields.Str()
    dte_creationdate = fields.Date()
